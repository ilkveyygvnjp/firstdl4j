package video;

import face.LBFWrapper;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.opencv.opencv_core.*;
import utils.Utils;



public class Video {
    private FrameGrabber grabber;
    private static Frame frame;
    private CanvasFrame cFrame;
    private OpenCVFrameConverter.ToMat converterToMat = new OpenCVFrameConverter.ToMat();
    private LBFWrapper facemarker = new LBFWrapper();

    public Video() {
        try {
            this.frame = null;
            this.grabber = FrameGrabber.createDefault(0);
            this.cFrame = new CanvasFrame("Farme", CanvasFrame.getDefaultGamma() / grabber.getGamma());
        } catch (Exception e) {
            Utils.err("Error, Finish this Program");
            System.exit(0);
        }
    }
    public void capture() throws Exception {
        this.grabber.start();
        while ((this.frame = this.grabber.grab()) != null) {
            if (this.cFrame.isVisible()) {
                Mat image = converterToMat.convert(frame);
                facemarker.setImage(image);
                facemarker.fit();
                this.cFrame.showImage(converterToMat.convert(image));
            }
        }
    }
}
