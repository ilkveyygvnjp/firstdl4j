package SSD;

import com.github.chen0040.objdetect.ObjectDetector;
import com.github.chen0040.objdetect.models.DetectedObj;
import utils.Utils;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

public class SSD {
    private static Utils utils = new Utils();
    private BufferedImage img;
    private ObjectDetector detector;
    public SSD() {
        try {
            this.detector = new ObjectDetector();
            this.detector.loadModel();
        }catch (Exception e){
        }
    }
    public void setImage(String path) {
        try{
            this.img = ImageIO.read(new File(utils.resource(path)));
            List<DetectedObj> result = detector.detectObjects(this.img);
            System.out.println(result);
        }catch (Exception e) {
        }
    }
}
