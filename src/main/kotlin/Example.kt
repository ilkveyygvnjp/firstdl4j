
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator
import org.deeplearning4j.eval.Evaluation
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.MultiLayerConfiguration
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.Updater
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.learning.config.Nesterovs
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class  Example {

    private val log = LoggerFactory.getLogger(Example::class.java)

    @Throws(Exception::class)
    fun test() {
        val numRows = 28
        val numColumns = 28
        val outputNum = 10
        val batchSize = 64
        val rngSeed = 123
        val numEpochs = 15
        val rate = 0.0015


        val mnistTrain = MnistDataSetIterator(batchSize, true, rngSeed)
        val mnistTest = MnistDataSetIterator(batchSize, false, rngSeed)

        println(mnistTrain)
        println(mnistTest)


        println("Build model....")
        val conf = NeuralNetConfiguration.Builder()
                .seed(rngSeed.toLong())
                .activation(Activation.RELU)
                .weightInit(WeightInit.XAVIER)
                .updater(Nesterovs(rate, 0.98))
                .l2(rate * 0.005)
                .list()
                .layer(DenseLayer.Builder()
                        .nIn(numRows * numColumns)
                        .nOut(500)
                        .build())
                .layer(DenseLayer.Builder()
                        .nIn(500)
                        .nOut(100)
                        .build())
                .layer(OutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD)
                        .activation(Activation.SOFTMAX)
                        .nIn(100)
                        .nOut(outputNum)
                        .build())
                .build()

        val model = MultiLayerNetwork(conf)
        model.init()

        println("Train model....")
        model.setListeners(ScoreIterationListener(5), org.deeplearning4j.optimize.listeners.EvaluativeListener(mnistTest, 300))
        model.fit(mnistTrain, numEpochs)


        println("Evaluate model....")
        val eval: org.nd4j.evaluation.classification.Evaluation = model.evaluate(mnistTest)
        println(eval.stats())

        println("****************Example finished********************")
    }
}