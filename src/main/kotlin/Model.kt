
import org.datavec.api.records.reader.RecordReader
import org.datavec.api.records.reader.impl.csv.CSVRecordReader
import org.datavec.api.split.FileSplit
import org.datavec.api.util.ClassPathResource
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator
import org.deeplearning4j.nn.conf.MultiLayerConfiguration
import org.nd4j.linalg.activations.Activation
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.nd4j.evaluation.classification.Evaluation
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.dataset.SplitTestAndTrain
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize
import org.nd4j.linalg.learning.config.Nesterovs
import org.nd4j.linalg.learning.config.Sgd
import org.nd4j.linalg.lossfunctions.LossFunctions

class Model {
    @Throws(Exception::class)
    fun start() {

        val numLineToSkip = 0
        val delimiter = ','

        val recordReader: RecordReader = CSVRecordReader(numLineToSkip, delimiter)
        recordReader.initialize(FileSplit(ClassPathResource("iris.txt").file))

        val labelIndex = 4
        val numClasses = 3
        val batchSize = 150 // batch size for each epoch

        val iterator: DataSetIterator = RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, numClasses)
        val allData: DataSet = iterator.next()
        allData.shuffle()
        val testAndTrain: SplitTestAndTrain = allData.splitTestAndTrain(0.65)

        val trainingData: DataSet = testAndTrain.train
        val testData: DataSet = testAndTrain.test

        val normalizer: DataNormalization = NormalizerStandardize()
        normalizer.fit(trainingData)
        normalizer.transform(trainingData)
        normalizer.transform(testData)

//        val numRows = 28
//        val numColumns = 28
        val numInputs = 4
        val outputNum = 3 // number of output classes
        //val rngSeed = 123 // random number seed for reproducibility
        //val numEpochs = 15 // number of epochs to perform
        val rate = 0.0015 // learning rate
        var seed: Long = 123
        val conf: MultiLayerConfiguration = NeuralNetConfiguration.Builder()
                .seed(seed)
                .activation(Activation.RELU)
                .weightInit(WeightInit.XAVIER)
                .updater(Sgd(0.1)) //specify the rate of change of the learning rate.
                .l2(1e-4) // regularize learning model
                .list()
                .layer(DenseLayer.Builder() //create the first input layer.
                        .nIn(numInputs)
                        .nOut(3)
                        .build())
                .layer(DenseLayer.Builder() //create the second input layer
                        .nIn(3)
                        .nOut(3)
                        .build())
                .layer(OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD) //create hidden layer
                        .activation(Activation.SOFTMAX)
                        .nIn(3)
                        .nOut(outputNum)
                        .build())
                .build()

        var model = MultiLayerNetwork(conf)
        model.init()
        model.setListeners(ScoreIterationListener(100))

        for(i in 0..1000) {
            model.fit(trainingData)
        }

        val eval = Evaluation(3)
        val output: INDArray = model.output(testData.features)
        eval.eval(testData.labels, output)
        println(eval.stats())
    }
}