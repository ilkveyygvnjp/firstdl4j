import org.datavec.api.records.reader.RecordReader
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.dataset.SplitTestAndTrain
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator

class DataManeger {
    fun getData(iterator: CSVInterator): Array<DataSet> {
        val allData: DataSet = iterator.iterator.next()
            allData.shuffle()
        val testAndTrain: SplitTestAndTrain = allData.splitTestAndTrain(0.65)
        val trainingData: DataSet = testAndTrain.train
        val testData: DataSet = testAndTrain.test
        return arrayOf(trainingData, testData)

    }
    class  CSVInterator {
        lateinit var iterator: DataSetIterator
        constructor(recordReader: RecordReader, batchSize: Int, labelIndex: Int, numClasses: Int){
            this.iterator = RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, numClasses)

        }
    }
}