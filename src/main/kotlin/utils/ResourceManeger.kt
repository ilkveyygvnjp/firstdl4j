import org.datavec.api.records.reader.RecordReader
import org.datavec.api.records.reader.impl.csv.CSVRecordReader
import org.datavec.api.split.FileSplit
import org.nd4j.linalg.io.ClassPathResource

class ResourceManeger {
    companion object {
        fun getCSV(fileName: String): RecordReader {
            val recordReader: RecordReader = CSVRecordReader(0)
            recordReader.initialize(FileSplit(ClassPathResource(fileName).file))
            return recordReader
        }
    }
}