import org.bytedeco.javacv.CanvasFrame
import org.bytedeco.javacv.Frame
import org.bytedeco.javacv.OpenCVFrameConverter
import org.bytedeco.javacv.OpenCVFrameGrabber
import org.opencv.core.Core
import org.opencv.imgproc.Imgproc
import java.lang.Exception
import javax.swing.JFrame

class Capture {
    fun start() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
        // ウィンドウの生成
        val canvas = CanvasFrame("capture")
        canvas.defaultCloseOperation = JFrame.EXIT_ON_CLOSE

        // Webカメラ取得
        val grabber = OpenCVFrameGrabber(0)

        try {
            // Webカメラのキャプチャ開始
            grabber.start()

            val frameRate: Double = grabber.frameRate
            val wait : Long = ((1000 / if (frameRate == 0.0) 10.0 else frameRate)).toLong()

            // 最初と現在のフレームの移動平均
            var avg: org.opencv.core.Mat? = null

            while (true) {
                Thread.sleep(wait)

                // Webカメラからフレームを取得してグレースケールに変換
                val frame:Frame = grabber.grab()
                val mat: org.opencv.core.Mat = OpenCVFrameConverter.ToIplImage().convertToOrgOpenCvCoreMat(frame)
                Imgproc.cvtColor(mat,mat,Imgproc.COLOR_RGB2GRAY)
                val gray = mat.clone()

                // 現在が1フレーム目ならコピーしてフレーム再取得
                if (avg == null){
                    avg = gray.clone()
                    continue
                }

                canvas.showImage(OpenCVFrameConverter.ToMat().convert(gray))

//                // 最初と現在のフレームの移動平均を計算しデルタ画像生成
//                Imgproc.accumulateWeighted(gray, avg, 0.5)
//                var imgDelta: org.opencv.core.Mat? = null
//                Core.absdiff(gray, avg, imgDelta)
//
//                // デルタ画像を閾値処理
//                var imgThresh: org.opencv.core.Mat? = null
//                Imgproc.threshold(imgDelta, imgThresh, 3.0, 255.0, Imgproc.THRESH_BINARY)
//
//                val threshFrame: Frame = OpenCVFrameConverter.ToMat().convert(imgThresh)
//                canvas.showImage(threshFrame)

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}