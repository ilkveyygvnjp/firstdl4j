import face.KazemiWrapper
import face.LBFWrapper
import utils.Utils;
import java.util.*


class Script {
    private val utils = Utils()
    private val sc: Scanner = Scanner(System.`in`)
    fun run() {
        val lbf = LBFWrapper()
        lbf.setImage(utils.resource("/opencv/images/def.jpg"))
        lbf.fit()
        lbf.showFacemark()
        /*
        val kw = KazemiWrapper()
        while(sc.hasNext()){
            val name: String = sc.next()
            if (name.equals("exit")) break
            kw.setImage(name)
            kw.fit()
            val points = kw.list[0]
            utils.info<String>(points.x.toString() + "//" + points.y.toString())
            kw.showFacemark()
        }*/

    }
}