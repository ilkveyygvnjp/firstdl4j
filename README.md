# first init dl4j
First input

#ND4J
> ND4Jは、行列等のN次元配列を簡単に扱えるようにするための数値計算ライブラリです。Python用に存在している数値計算ライブラリのNumPyを参考して作られたようです。  
[ND4J](http://www.torutk.com/projects/swe/wiki/ND4J)

#numpy
> NumPyは、プログラミング言語Pythonにおいて数値計算を効率的に行うための拡張モジュールである。効率的な数値計算を行うための型付きの多次元配列（例えばベクトルや行列などを表現できる）のサポートをPythonに加えるとともに、それらを操作するための大規模な高水準の数学関数ライブラリを提供する。  
[numpy](https://qiita.com/Mskmemory/items/3ded3d0d19ab7741cd8f)

#パーセプトロン
> パーセプトロンは、複数の入力に対して1つ出力する関数です.出力は1か0の2値です。  
[パーセプトロン](https://qiita.com/yudsuzuk/items/a8e1eee92403f0921d92)

#DL4J  
ライセンス：Apache 2.0  (https://qiita.com/0xfffffff7/items/efbb65521d7708f2db7d)  
|Required(必須)  | Permitted(許可) |Forbidden(禁止)  |
|---|---|---|
|著作権の表示、変更箇所の明示  |商用利用、修正、配布、サブライセンス、特許許可  |トレードマークの使用、責任免除 |
（https://qiita.com/tukiyo3/items/58b8b3f51e9dc8e96886）




